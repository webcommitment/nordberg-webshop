<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header();

$blog = get_queried_object();
$blog_id = $blog->ID;

?>
    <article id="page-blog">
        <section class="page-banner" style="background-image: url('<?php echo get_the_post_thumbnail_url($blog_id); ?>');">
            <div class="banner-inner">
                <div class="banner-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-11 col-xl-10">
                                <div class="content">
                                    <h1>
										<?php echo get_the_title($blog_id); ?>
                                    </h1>
                                    <p>
										<?php echo get_the_excerpt($blog_id); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<?php get_template_part( 'template-parts/content', 'newsarchive' ); ?>
		<?php get_template_part( 'template-parts/content', 'galerij' ); ?>
    </article>
<?php
get_footer();
