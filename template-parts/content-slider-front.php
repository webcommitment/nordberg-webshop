<?php
$posts_2 = get_field('slider_content_front'); ?>
<?php if ($posts_2) : ?>

    <div id="headerFrontCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach ($posts_2
                as $index => $post) :
            ?>
                <?php $bg = get_field('slider_bg', $post->ID); ?>
                <div class="carousel-item <?php echo ($index == 0) ? 'active' : '' ?>" style="background-image: url('<?php echo $bg['url'] ?>');">
                    <?php
                    $title = get_field('slider_title', $post->ID);
                    $content = get_field('slider_content', $post->ID);
                    $btn_link = get_field('slider_button_link', $post->ID);
                    $btn_text = get_field('slider_button_tekst', $post->ID);
                    $post_id = $post->ID;
                    if ($title || $btn_text) : ?>
                        <div class="row row-slider justify-content-center">
                            <div class="col-10 col-slider">
                                <div class="slider-wrap">
                                    <div class="slider-inner">
                                        <?php if ($title) : ?>
                                            <div class="slider-title">
                                                <?php echo $title; ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($content) : ?>
                                            <div class="slider-content">
                                                <p>
                                                    <?php echo $content; ?>
                                                </p>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($btn_text) : ?>
                                            <div class="slider-cta">
                                                <a href="<?php echo $btn_link['url']; ?>" class="secondary-btn">
                                                    <?php echo $btn_text; ?> </a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <ol class="carousel-indicators">
            <?php foreach ($posts_2
                as $index => $post) :
            ?>
                <li data-target="#headerFrontCarousel" data-slide-to=<?php echo $index++; ?>>
                </li>

            <?php endforeach; ?>
        </ol>

    </div>
<?php endif; ?>