<?php
if (is_page('contact')) :
    $option_adres = get_field('option_adres', 'option');
    $option_postcode = get_field('option_postcode', 'option');
    $option_stad = get_field('option_stad', 'option');
    $option_tel = get_field('option_tel', 'option');
    $option_mail = get_field('option_mail', 'option');
    $option_kvk = get_field('option_kvk', 'option');
    ?>
    <section id="page-contact" class="page page-contact">
        <div class="container-fluid">
            <div class="row justify-content-center justify-content-xl-start">
                <div class="col-12 col-md-11 col-xl-10 offset-xl-1">
                    <div class="contact-content">
                        <?php
                        while (have_posts()) : the_post();
                            the_content();
                        endwhile; // End of the loop.
                        ?>
                    </div>
                    <div class="contact-info">
                        <ul>
                            <li>
                                <?php echo $option_adres; ?>
                            </li>
                            <li>
                                <?php echo $option_postcode . ' ' . $option_stad; ?>
                            </li>
                            <li>
                                <a class="unset" href="tel:<?php echo $option_tel; ?>">
                                    <?php echo __('T:', 'webcommitment-theme') . ' ' . $option_tel; ?>
                                </a>
                            </li>
                            <li>
                                <a class="unset" href="mailto:<?php echo $option_mail; ?>">
                                    <?php echo __('E:', 'webcommitment-theme') . ' ' . $option_mail; ?>
                                </a>
                            </li>gti
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pl-0 pr-0">
                    <div class="cf-wrapper">
                        <div class="container-fluid">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-11 col-xl-10">
                                    <div class="cf">
                                        <?php echo do_shortcode('[contact-form-7 id="5" title="Contactformulier NL"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php else : ?>
    <?php $content_img = get_field('page_content_img'); ?>
    <section id="page" class="page">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-11 col-xl-10">
                    <div class="row">
                        <div class="col-12 col-md-11 col-lg-7">
                            <?php if (is_single()) : ?>
                                <div class="single-misc">
                                <span class="date">
                                    <?php echo get_the_date('d M Y'); ?>
                                </span>
                                    <?php
                                    $postcat = get_the_category($post_id); ?>
                                    <?php
                                    if (!empty($postcat)) {
                                        echo '<a href="/category/' . $postcat[0]->slug . '"><span class="cat">';
                                        echo esc_html($postcat[0]->name);
                                        echo '</span></a>';
                                    }
                                    ?>
                                </div>
                            <?php
                            endif;
                            while (have_posts()) : the_post();
                                the_content();
                            endwhile; // End of the loop.
                            ?>
                        </div>
                        <div class="col-12 pl-0 pr-0 pr-md-3 pl-md-3 col-lg-5">
                            <?php if ($content_img['url']): ?>
                                <div class="content-img">
                                    <img src="<?php echo $content_img['url']; ?>"
                                         alt="<?php echo $content_img['alt']; ?>">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php if (is_page_template('page-parent.php')) : ?>
            <?php get_template_part('template-parts/content', 'page-cta'); ?>
        <?php endif; ?>
    <article id="front-page">
        <section id="section-slider">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pl-0 pr-0">
                        <?php get_template_part('template-parts/content', 'slider'); ?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php endif; ?>
