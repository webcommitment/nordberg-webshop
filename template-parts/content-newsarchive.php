<section id="blogarchive" class="page">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-xl-10">
                <div class="page-parent-intro">
				    <?php
				    $blog = get_queried_object();
				    $content = apply_filters('the_content', $blog->post_content);
				    echo $content;
				    ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-xl-10">
			    <?php get_template_part( 'template-parts/child', 'posts' ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pl-0 pr-0 mt-4">
			    <?php get_template_part( 'template-parts/content', 'page-cta' ); ?>
            </div>
        </div>
    </div>
</section>
