<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

$children = get_pages( array( 'child_of' => $post->ID ) );

?>

<section id="page-parent" class="page">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-11 col-xl-10">
				<div class="page-parent-intro">
					<?php
					while ( have_posts() ) : the_post();
						the_content();
					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-md-11 col-xl-10">
				<?php get_template_part( 'template-parts/child', 'items' ); ?>
			</div>
		</div>
	</div>

</section>
<?php get_template_part( 'template-parts/content', 'page-cta' ); ?>
