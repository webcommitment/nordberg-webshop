<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 09/07/2020
 * Time: 13:41
 */ ?>

<?php

// first we need to get the product categories ....

$categories_args = array(
    'taxonomy' => 'product_cat' // the taxonomy we want to get terms of
);

$product_categories = get_terms($categories_args); // get all terms of the product category taxonomy

if ($product_categories) : ?>
    <section id="page-woo" class="page">

        <div class="container-fluid">

            <div class="row justify-content-center">

                <div class="col-12 col-md-11 col-xl-10">

                    <div class="row">
                        <?php foreach ($product_categories as $product_category): ?>

                            <?php
                            $term_id = $product_category->term_id; // Term ID
                            $term_name = $product_category->name; // Term name
                            $term_desc = $product_category->description; // Term description
                            $term_link = get_term_link($product_category->slug, $product_category->taxonomy); // Term link\
                            $thumbnail_id = get_term_meta($product_category->term_id, 'thumbnail_id', true);
                            $term_thumb = wp_get_attachment_url($thumbnail_id); ?>
                            <div class="col-sm-6 col-lg-4 col-card visibility is-visible">
                                <div class="content-card">
                                    <div class="card-thumb">
                                        <a href="<?php echo $term_link; ?>">
                                            <img src="<?php echo $term_thumb; ?>" alt="<?php echo $term_title; ?>">
                                        </a>
                                    </div>
                                    <div class="card-title">
                                        <h2 class="product-cat-title"><a
                                                    href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a>
                                        </h2>
                                    </div>
                                    <div class="card-excerpt">
                                        <p>
                                            <?php echo $term_desc; ?>
                                        </p>
                                    </div>
                                    <div class="card-cta">
                                        <a href="<?php echo $term_link; ?>" class="primary-btn">
                                            <?php echo __('SEE MORE', 'webcommitment-theme'); ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
?>



