<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 16/10/2019
 * Time: 14:42
 */

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'page',
	'orderby'        => 'menu_order',
	'post__not_in'     => array(150,),
	'meta_query'     => array(
		array(
			'key'   => '_wp_page_template',
			'value' => 'page-parent.php',
		)
	)
);

$children = get_pages( array(
        'child_of' => $post->ID,
        'sort_column' => 'menu_order'
) );

$child_count = count($children);

$col = 'col-sm-6 col-lg-4';

if ( $children ){
    $pages = $children;
    if ( $child_count === 2 || $child_count === 4) {
        $col = 'col-6';
    }
}

else{
	$pages = get_posts( $args );
}

?>

<div class="row">
	<?php
	foreach ( $pages as $page ) :
		$post_id = $page->ID;
		$post_title = $page->post_title;
		$post_thumb = get_the_post_thumbnail_url( $post_id );
		if ( !$post_thumb ){
		    $post_thumb = '/wp-content/themes/webcommitment-theme/img/placeholder_thumb.jpg';
        }
		$post_excerpt = $page->post_excerpt;
		$post_link = get_the_permalink( $post_id ); ?>

        <div class="<?php echo $col; ?> col-card visibility">
            <div class="content-card">
                <div class="card-title">
                    <h2>
						<?php echo $post_title; ?>
                    </h2>
                </div>
                <div class="card-thumb">
                    <a href="<?php echo $post_link; ?>">
                        <img src="<?php echo $post_thumb; ?>" alt="<?php echo $post_title; ?>">
                    </a>
                </div>
                <div class="card-excerpt">
					<p>
						<?php echo $post_excerpt; ?>
                    </p>
                </div>
                <div class="card-cta">
                    <a href="<?php echo $post_link; ?>" class="primary-btn">
	                    <?php echo __('BEKIJKEN', 'webcommitment-theme'); ?>
                    </a>
                </div>
            </div>
        </div>
	<?php endforeach; ?>
</div>
