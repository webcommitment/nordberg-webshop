<?php
$usps_1 = get_field('icon_usps_1', 'option');
$usps_2 = get_field('icon_usps_2', 'option');
$usps_3 = get_field('icon_usps_3', 'option');
?>
<aside class="page-cta-wrapper text-center">
    <div class="bucket"></div>
    <div class="bucket"></div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-4 col-md-3 col-lg-2">
                <div class="cta-icon">
                    <img src="<?php echo $usps_1['icon_usps']['url']; ?>"
                         alt="<?php echo $usps_1['icon_usps']['alt']; ?>">
                </div>

                <div class="cta-desc">
                    <div class="cta-subtitle">
                        <?php echo $usps_1['subtitle_usps']; ?>
                    </div>
                    <div class="cta-title">
                        <?php echo $usps_1['title_usps']; ?>
                    </div>
                </div>

            </div>
            <div class="col-12 col-sm-4 col-md-3 col-lg-2">
                <div class="cta-icon">
                    <img src="<?php echo $usps_2['icon_2_usps']['url']; ?>"
                         alt="<?php echo $usps_2['icon_2_usps']['alt']; ?>">
                </div>

                <div class="cta-desc">
                    <div class="cta-subtitle">
                        <?php echo $usps_2['subtitle_2_usps']; ?>
                    </div>
                    <div class="cta-title">
                        <?php echo $usps_2['title_2_usps']; ?>
                    </div>
                </div>

            </div>
            <div class="col-12 col-sm-4 col-md-3 col-lg-2">

                <div class="cta-icon">
                    <img src="<?php echo $usps_3['icon_3_usps']['url']; ?>"
                         alt="<?php echo $usps_3['icon_3_usps']['alt']; ?>">
                </div>
                <div class="cta-desc">
                    <div class="cta-subtitle">
                        <?php echo $usps_3['subtitle_3_usps']; ?>
                    </div>
                    <div class="cta-title">
                        <?php echo $usps_3['title_3_usps']; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="bucket"></div>
    <div class="bucket"></div>
</aside>
