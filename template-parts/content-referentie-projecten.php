<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 25/10/2019
 * Time: 10:19
 */

if ( have_rows( 'referentie_projecten' ) ): $j = 0; ?>

<section id="referentie-projecten">
	<div id="refCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<?php while ( have_rows( 'referentie_projecten' ) ): the_row();
				$ref_achtergrond   = get_sub_field( 'ref_achtergrond' );
				$ref_content = get_sub_field( 'ref_content' ); ?>
				<div class="carousel-item <?php echo ( $j == 0 ) ? 'active' : '' ?>" style="background-image: url('<?php echo $ref_achtergrond; ?>');">
					<div class="ref-content">
						<?php echo $ref_content; ?>
					</div>
				</div>
				<?php $j++; endwhile; ?>
		</div>
		<a class="carousel-control-prev disable-anim" href="#refCarousel" role="button" data-slide="prev">
			<?php get_template_part( 'img/icons/arrow-left.svg' ); ?>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next disable-anim" href="#refCarousel" role="button" data-slide="next">
			<?php get_template_part( 'img/icons/arrow-left.svg' ); ?>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>

<?php endif;
