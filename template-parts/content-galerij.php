<?php

/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 16/10/2019
 * Time: 16:03
 */

$galerij_titel = get_field('galerij_titel');
if (!$galerij_titel) {
    $galerij_titel = 'Impressie van de mogelijkheden';
}
$images = get_field('galerij_afbeeldingen');
if ($images && is_array($images)) {
    $img_count = count($images);
} else {
    $img_count = 0;
}

if ($images) : ?>

    <section class="section-galerij">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-11 col-xl-10">
                    <h2 class="light-title impressie-title">
                        <?php echo $galerij_titel; ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-11 col-xl-10">
                    <div class="section-galerij__wrapper">
                        <?php foreach ( $images as $image ) : ?>
                            <div class="section-galerij__item">
                                <img src="<?php echo $image['sizes']['large']; ?>" data-fancybox="gallery" data-caption="<?php echo $image['description'] ?>" href="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>