<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 16/10/2019
 * Time: 14:42
 */

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'page',
	'orderby'        => 'menu_order',
	'post__not_in'     => array(150,),
	'meta_query'     => array(
		array(
			'key'   => '_wp_page_template',
			'value' => 'page-parent.php',
		)
	)
);

$col = 'col-sm-6 col-lg-4';

if ( have_rows('uitgelichte_paginas') ) : ?>

    <div class="row">
		<?php
		while ( have_rows('uitgelichte_paginas') ) : the_row();
			$pagina_id = get_sub_field('pagina');


			$post_id = $pagina_id;
			$post_title = get_the_title($pagina_id);
			$post_thumb = get_the_post_thumbnail_url( $pagina_id );
			if ( !$post_thumb ){
				$post_thumb = '/wp-content/themes/webcommitment-theme/img/placeholder_thumb.jpg';
			}
			$post_excerpt = get_the_excerpt( $pagina_id );
			$post_link = get_the_permalink( $pagina_id ); ?>

            <div class="<?php echo $col; ?> col-card visibility">
                <div class="content-card">
                    <div class="card-title">
                        <h2>
							<?php echo $post_title; ?>
                        </h2>
                    </div>
                    <div class="card-thumb card-thumb--front">
                        <a href="<?php echo $post_link; ?>">
                            <img src="<?php echo $post_thumb; ?>" alt="<?php echo $post_title; ?>">
                        </a>
                    </div>
                    <div class="card-excerpt">
                        <p>
							<?php echo $post_excerpt; ?>
                        </p>
                    </div>
                    <div class="card-cta">
                        <a href="<?php echo $post_link; ?>" class="primary-cta ">
							<?php echo __('Read more', 'webcommitment-theme'); ?>
                        </a>
                    </div>
                </div>
            </div>

		<?php endwhile; ?>
    </div>
<?php endif; ?>