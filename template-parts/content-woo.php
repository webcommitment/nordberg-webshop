<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 09/07/2020
 * Time: 13:41
 */ ?>

<section id="page-woo" class="page">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-11 col-xl-10">
				<div class="row">
					<div class="col-12 col-md-12">
                        <div class="woo-product-wrapper">
	                        <?php
	                        while ( have_posts() ) : the_post();
		                        the_content();
	                        endwhile; // End of the loop.
	                        ?>
                        </div>
					</div>
				</div>
			</div>
			<div class="col-12 pl-0 pr-0">
				<?php get_template_part( 'template-parts/content', 'page-cta' ); ?>
			</div>
		</div>
	</div>
</section>
