<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 16/10/2019
 * Time: 14:42
 */

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'post',
	'orderby'        => 'date',
	'order' => 'DESC'
);

if ( is_category() ){
	$blog = get_queried_object();
	$blog_id = $blog->cat_id;

	$args = array(
		'cat' => get_query_var('cat'),
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page'=>-1,
		'caller_get_posts'=>1
	);
}

$children = get_posts($args);

$child_count = count($children);

$col = 'col-sm-6 col-lg-4';

if ( $children ){
	$pages = $children;
	if ( $child_count === 2 || $child_count === 4) {
		$col = 'col-6';
	}
}

else{
	echo 'Er zijn nog geen nieuwsberichten.';
}

?>

<div class="row">
	<?php
	foreach ( $pages as $page ) :
		$post_id = $page->ID;
		$post_title = $page->post_title;
		$post_thumb = get_the_post_thumbnail_url( $post_id );
		if ( !$post_thumb ){
			$post_thumb = '/wp-content/themes/webcommitment-theme/img/placeholder_thumb.jpg';
		}
		$post_excerpt = $page->post_excerpt;
		$post_link = get_the_permalink( $post_id ); ?>

        <div class="<?php echo $col; ?> col-card">
            <div class="content-card">
                <div class="card-thumb">
                    <a href="<?php echo $post_link; ?>">
                        <img src="<?php echo $post_thumb; ?>" alt="<?php echo $post_title; ?>">
                    </a>
                    <span class="date">
                        <?php echo get_the_date('d M Y', $post_id); ?>
                    </span>
					<?php
					$postcat = get_the_category( $post_id ); ?>
					<?php
					if ( ! empty( $postcat ) ) {
						echo '<a href="/category/'.$postcat[0]->slug.'"><span class="cat">';
						echo esc_html( $postcat[0]->name );
						echo '</span></a>';
					}
					?>
                </div>
                <div class="card-title">
                    <h2>
						<?php echo $post_title; ?>
                    </h2>
                </div>
                <div class="card-excerpt">
                    <p>
						<?php echo $post_excerpt; ?>
                    </p>
                </div>
                <div class="card-cta">
                    <a href="<?php echo $post_link; ?>" class="primary-btn">
						<?php echo __('Read more', 'webcommitment-theme'); ?>
                    </a>
                </div>
            </div>
        </div>
	<?php endforeach; ?>
</div>
