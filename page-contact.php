<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 *  Template Name: Page-contact
 *  Template Post Type: page
 */

$option_adres    = get_field( 'option_adres', 'option' );
$option_stad     = get_field( 'option_stad', 'option' );

get_header(); ?>
	<article>
		<section class="page-banner" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></section>
		<?php
        get_template_part( 'template-parts/content', 'page' );
		get_template_part( 'template-parts/content', 'galerij' );
		$address =  $option_adres.', '.$option_stad;
		echo '<iframe width="100%" height="500" frameborder="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed"></iframe>';
		?>
	</article>
<?php
get_footer();
