<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package webcommitment_Starter
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function webcommitment_starter_body_classes($classes)
{
    // Adds a class of group-blog to blogs with more than 1 published author.
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    return $classes;
}

add_filter('body_class', 'webcommitment_starter_body_classes');


add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);
function woo_remove_product_tabs($tabs)
{
    unset($tabs['reviews']);          // Remove the reviews tab
    return $tabs;
}

//
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 5);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
//add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 5 );

add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text');
function woocommerce_custom_single_add_to_cart_text()
{
    return __('ENQUIRE', 'woocommerce');
}

function slider_init()
{
    $labels = array(
        'name' => _x('Slides', 'post type general name', 'theme core'),
        'singular_name' => _x('Slide', 'post type singular name', 'theme core'),
        'menu_name' => _x('Slides', 'admin menu', 'theme core'),
        'name_admin_bar' => _x('Slides', 'add new on admin bar', 'theme core'),
        'add_new' => _x('Nieuwe slide', 'theme core'),
        'add_new_item' => __('Nieuwe slide toevoegen', 'theme core'),
        'new_item' => __('Nieuwe slide', 'theme core'),
        'edit_item' => __('Edit slide', 'theme core'),
        'view_item' => __('Bekijk slide', 'theme core'),
        'all_items' => __('Alle slides', 'theme core'),
        'search_items' => __('Zoek slides:', 'theme core'),
        'not_found' => __('Geen slides gevonden', 'theme core'),
        'not_found_in_trash' => __('Geen slides gevonden in de prullenbak', 'theme core')
    );
    $args = array(
        'labels' => $labels,
        'description' => __('Description', 'theme core'),
        'menu_icon' => 'dashicons-slides',
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slides'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title'),
        'taxonomies' => false
    );
    register_post_type('slides', $args);
}

add_action('init', 'slider_init');
