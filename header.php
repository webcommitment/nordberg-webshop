<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!--    get analytics script-->
    <?php get_template_part('template-parts/analytics/content', 'head'); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--    get analytics script-->
<?php get_template_part('template-parts/analytics/content', 'body'); ?>

<?php
$user_is_loggedin = true;
$current_user = wp_get_current_user();
$current_name = $current_user->display_name;
if (!is_user_logged_in()) {
    $user_is_loggedin = false;
}
?>
<div id="main-page" class="site">
    <header id="masthead" class="site-header" role="banner">
        <div id="fixed-header" <?php if (!is_front_page()) {
            echo 'class="page-header"';
        } ?>>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-11 col-xl-10 align-self-center">
                        <div class="row">
                            <div class="header-inner">
                                <figure class="site-branding">
                                    <div class="brand-wrapper">
                                        <?php the_custom_logo(); ?>
                                    </div>
                                </figure><!-- .site-branding -->
                                <div class="nav-wrapper">
                                    <!--                                    <div class="language-switch">-->
                                    <!--										--><?php //do_action('wpml_add_language_selector'); ?>
                                    <!--                                    </div>-->
                                    <div class="nav-wrapper-inner">
                                        <div class="nav-user">
                                            <!--											--><?php //if ( $user_is_loggedin ) : ?>
                                            <!--												-->
                                            <?php //echo __('Hallo', 'webcommitment-theme'); ?><!-- <strong>-->
                                            <?php //echo $current_name; ?><!--</strong> (<a href="-->
                                            <?php //echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?><!--">-->
                                            <?php //echo __('Mijn account', 'webcommitment-theme'); ?><!--</a>)-->
                                            <!--											--><?php //else : ?>
                                            <!--                                                <a href="-->
                                            <?php //echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?><!--">-->
                                            <!--													--><?php //echo __('Inloggen', 'webcommitment-theme'); ?>
                                            <!--                                                </a>-->
                                            <!--											--><?php //endif; ?>
                                        </div>
                                        <div class="cta-phone">
                                            <?php $option_tel = get_field('header_cta_button', 'option'); ?>
                                            <a href="<?php echo $option_tel['url']; ?>" class="primary-cta">
                                                <!--												--><?php //get_template_part( 'img/icons/phone.svg' );
                                                echo $option_tel['title'];
                                                ?>
                                            </a>
                                        </div>
                                        <div class="instagram">
                                            <?php $insta = get_field('option_instagram', 'options'); ?>
                                            <?php if ($insta) : ?>
                                                <a href="<?php echo $insta; ?>" class="">
                                                    <?php get_template_part('img/icons/instagram.svg');
                                                    ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="hamgebur">
                                            <button class="hamburger hamburger--elastic" type="button">
                                          <span class="hamburger-box">
                                            <span class="hamburger-inner"></span>
                                          </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav id="menu-overlay">
            <div class="overlay-inner">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-10 col-xl-8">
                            <div class="row">
                                <?php $footer_menu_items = wp_get_nav_menu_items('menu-1'); ?>
                                <div class="main-menu">
                                    <?php
                                    foreach ($footer_menu_items as $footer_item) :
                                        $menu_url = $footer_item->url;
                                        $menu_name = $footer_item->title;
                                        $object_id = get_post_meta($footer_item->ID, '_menu_item_object_id', true);
                                        ?>
                                        <div class="menu-item">
                                            <div class="menu-name">
                                                <a href="<?php echo $menu_url; ?>">
                                                    <?php echo $menu_name; ?>
                                                </a>
                                            </div>
                                            <div class="menu-img">
                                                <a href="<?php echo $menu_url; ?>">
                                                    <img src="<?php echo get_the_post_thumbnail_url($object_id); ?>"
                                                         alt="">
                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header><!-- #masthead -->
    <main>
        <div id="content" class="site-content">