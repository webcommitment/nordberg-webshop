<?php
/**
 * The template for displaying the front-page
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
    <article id="front-page">
        <section id="front-banner" class="main-slider">
            <?php get_template_part('template-parts/content', 'slider-front'); ?>
        </section>
        <section id="section-2" class="banner-wrapper">
            <div class="scroll-down">
                <img src="/wp-content/themes/webcommitment-theme/img/icons/scroll.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-11 col-xl-10 text-center">
                        <div class="front-intro">
                            <?php
                            if (have_posts()):
                                while (have_posts()): the_post();
                                    the_content();
                                endwhile;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-11 col-xl-10">
                        <div class="page-child-wrapper">
                            <?php get_template_part('template-parts/child', 'items-front'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-slider">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pl-0 pr-0">
                        <?php get_template_part('template-parts/content', 'slider'); ?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php get_template_part('template-parts/content', 'page-cta'); ?>
<?php
get_footer();
