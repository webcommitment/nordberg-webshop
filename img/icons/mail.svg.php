<svg xmlns="http://www.w3.org/2000/svg" width="26.745" height="26.745" viewBox="0 0 26.745 26.745">
  <g id="Group_515" data-name="Group 515" transform="translate(-1378 -969.132)">
    <path id="Path_946" data-name="Path 946" d="M544.238,149.98a13.372,13.372,0,1,1,13.372-13.372A13.388,13.388,0,0,1,544.238,149.98Zm0-23.291a9.919,9.919,0,1,0,9.919,9.919A9.93,9.93,0,0,0,544.238,126.689Z" transform="translate(847.134 845.897)" fill="#696969"/>
    <text id="_" data-name="@" transform="translate(1391 987)" fill="#5f5f5f" font-size="14" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600" letter-spacing="0.1em"><tspan x="-5.481" y="0">@</tspan></text>
  </g>
</svg>
