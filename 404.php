<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package webcommitment_Starter
 */

get_header();
$frontpage_id = get_option( 'page_on_front' );
?>
    <article>
        <section class="page-banner error-banner" style="background-image: url('<?php echo get_the_post_thumbnail_url($frontpage_id); ?>');"></section>
        <section id="error-404" class="page">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-10">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1>
                                    Error 404 - <?php echo __('Pagina niet gevonden', 'webcommitment-theme'); ?>
                                </h1>
                                <p class="e2">
									<?php echo __('De pagina die je probeert de bereiken bestaat niet meer!', 'webcommitment-theme'); ?>
									<?php echo __('Het kan zijn dat deze pagina is verwijderd, of er is een verkeerde url ingevoerd. Wij raden je aan om terug te gaan naar de homepagina.', 'webcommitment-theme'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
get_footer();
