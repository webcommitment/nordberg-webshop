(function ($) {

    function equalHeight(group) {
        let tallest = 0;
        group.each(function () {
            const thisHeight = jQuery(this).height();
            if (thisHeight > tallest) {
                tallest = thisHeight;
            }
        });
        group.height(tallest);
    }

    function updateColorOnLoad() {
        var product_title = $('.product-button-wrapper').data('title');
    
        // Haal de kleurparameter op uit de huidige URL, als deze aanwezig is
        var existingColorParam = window.location.search.match(/(\?|&)attribute_pa_color=([^&]*)/);
        var colorQueryParam = existingColorParam ? existingColorParam[2] : "";

        // Construeer de href voor de e-mail link
        var href = "mailto:info@nordbergoutdoor.com?subject=Enquiry: " + product_title + " " + colorQueryParam;
    
        // Stel de href in voor de e-mail button
        $('.product-button-wrapper .secondary-btn').attr('href', href);
    };

    $(document).ready(function () {

        updateColorOnLoad();

        $('#main-page').addClass('fade-in');

        $('button.hamburger').click(function () {
            $(this).toggleClass('is-active');
            $('nav#menu-overlay, header').toggleClass('is-open');
            $('#main-page, body, html').toggleClass('disable-overflow');
        });

        $('ul.products').removeClass('columns-4');

        $(".panel-heading").on("click", function () {
            $(this).find(".product-collapse__toggle").toggleClass("active");
        });

        function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top - 400;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }

        $(window).on('scroll', function () {
            $(".visibility").each(function () {
                if (isScrolledIntoView($(this))) {
                    $(this).addClass("is-visible");
                }
            });
        });

        $('a:not(".disable-anim")').click(function () {
            $('#main-page').addClass('fade-out');
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() >= 300) { // this refers to window
                $('header').addClass('is-sticky');
            } else {
                $('header').removeClass('is-sticky');
            }
        });

        $('.filter-toggle').click(function () {
            $('aside.archive-sidebar-wrapper').addClass('is-active');
            $('body').addClass('is-fixed');
            $('.brand-wrapper').addClass('disappear')
        });

        $('.sidebar-close-btn').click(function () {
            $('aside.archive-sidebar-wrapper').removeClass('is-active');
            $('body').removeClass('is-fixed');
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".shop-archive").offset().top - 50
            }, 500);
            $('.brand-wrapper').removeClass('disappear')
        });

        $('.carousel').carousel({
            interval: 3000
        });

        $('#pa_color').on('change', function () {
            var product_title = $('.product-button-wrapper').data('title');
            var value = $(this).find('option:selected').text();

            var href = "mailto:info@nordbergoutdoor.com?subject=Enquiry: " + product_title + " " + value;
            $('.product-button-wrapper .secondary-btn').attr('href', href);
        });

        $('.archive .woo-variation-raw-select').on('change', function () {
            var value = $(this).find('option:selected').val();
            var primaryBtn = $(this).closest('.wvs-archive-product-wrapper').find('.primary-btn');
            var imageLink = $(this).closest('.wvs-archive-product-wrapper').find('.woocommerce-LoopProduct-link');

            var href = primaryBtn.attr('href');
            console.log('Oude href:', href);

            if ( href.includes('attribute_pa_color=') ) {
                // Vervang de bestaande waarde
                var newHref = href.replace(/(attribute_pa_color=)[^\&]+/, '$1' + value);
            } else {
                // Voeg de parameter toe
                var separator = href.includes('?') ? '&' : '?';
                var newHref = href + separator + 'attribute_pa_color=' + value;
                $(this).closest('.wvs-archive-product-wrapper').find('.hover-img').addClass('is-variable');;
            }
            console.log('Nieuwe href:', newHref);

            primaryBtn.attr('href', newHref);
            imageLink.attr('href', newHref);
        });

        equalHeight(jQuery('.woocommerce-loop-product__title'));
        equalHeight(jQuery('.products li p'));
        equalHeight(jQuery('.products li .archive-variable-items'));
    });

})(jQuery);

