<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post;
$terms = get_the_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) {
	$product_cat_id = $term->term_id;
	$thumbnail_id = get_term_meta( $product_cat_id, 'thumbnail_id', true );
	$image_url = wp_get_attachment_url( $thumbnail_id ); // This variable is returing the product category thumbnail image URL.
}
get_header( 'shop' ); ?>

<article>
	<section id="single-product-page" class="page-banner page-banner-product-single" style="background-image: url('/wp-content/themes/webcommitment-theme/img/prod_banner.jpg');">
	</section>
	<section class="single-product-wrapper">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-md-11 col-xl-10">
                    <aside class="wc_breadcrumb">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb();
						}
						?>
                    </aside>
					<?php while ( have_posts() ) : ?>
						<?php the_post(); ?>

						<?php wc_get_template_part( 'content', 'single-product' ); ?>

					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
		</div>
	</section>

</article>
<?php
get_footer( 'shop' );
