<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;
$permalink = $product->get_permalink();
// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
?>
<li <?php wc_product_class('', $product); ?>>
    <?php
    /**
     * Hook: woocommerce_before_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_open - 10
     */
    do_action('woocommerce_before_shop_loop_item');

    /**
     * Hook: woocommerce_shop_loop_item_title.
     *
     * @hooked woocommerce_template_loop_product_title - 10
     */
    do_action('woocommerce_shop_loop_item_title');

    /**
     * Hook: woocommerce_before_shop_loop_item_title.
     *
     * @hooked woocommerce_show_product_loop_sale_flash - 10
     * @hooked woocommerce_template_loop_product_thumbnail - 10
     */
    add_action('woocommerce_before_shop_loop_item_title_wc-flash', 'woocommerce_show_product_loop_sale_flash', 10);
    add_action('woocommerce_before_shop_loop_item_title_wc-img', 'woocommerce_template_loop_product_thumbnail', 10);
    do_action('woocommerce_before_shop_loop_item_title_wc-flash'); ?>
    <?php
    // Check if the product has a gallery
    if ($product->get_gallery_image_ids()) {
	    // Get the gallery image IDs for the product
	    $gallery_image_ids = $product->get_gallery_image_ids();

	    // Get the first gallery image ID
	    $first_image_id = reset($gallery_image_ids);

	    // Get the URL of the first gallery image
	    $first_image_url = wp_get_attachment_url($first_image_id);

	    // Output the image or use it as needed
	    echo '<img class="hover-img" src="' . $first_image_url . '" alt="Product Image">';
    }
    ?>
    <?php do_action('woocommerce_before_shop_loop_item_title_wc-img'); ?>
    <?php 
    /**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
    ?>
    <div class="wvs-archive-product-wrapper">
        <div class="wvs-archive-product-image"></div>
    </div>
    <?php echo the_excerpt(); ?>
    <div class="card-cta mt-4">
        <a href="<?php echo $permalink; ?>" class="primary-btn">
            <?php echo __('SEE MORE', 'webcommitment-theme'); ?>
        </a>
    </div>
</li>
