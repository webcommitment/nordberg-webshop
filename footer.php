<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

$option_adres = get_field('option_adres', 'option');
$option_postcode = get_field('option_postcode', 'option');
$option_stad = get_field('option_stad', 'option');
$option_tel = get_field('option_tel', 'option');
$option_mail = get_field('option_mail', 'option');
$option_kvk = get_field('option_kvk', 'option');
$option_facebook = get_field('option_facebook', 'option');
$option_twitter = get_field('option_twitter', 'option');
$option_instagram = get_field('option_instagram', 'option');
$option_linkedin = get_field('option_linkedin', 'option');
$shop_id = get_option('woocommerce_shop_page_id');
$page_id = get_queried_object_id();
$thumb = get_the_post_thumbnail_url($page_id);
$frontpage_id = get_option('page_on_front');

if (is_shop()) {
    $thumb = get_the_post_thumbnail_url($shop_id);
}

if (is_product_category()) {
    $thumb = get_the_post_thumbnail_url($frontpage_id);
}


?>
</div><!-- #content -->
</main>
<footer style="background-image: url('<?php echo $thumb; ?>')">
    <div class="container-fluid pt-5 pb-5 mb-4">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-xl-10">
                <div class="row justify-content-between">
                    <div class="col-12 mb-4 text-center col-sm-6 mb-sm-0 text-sm-left col-md-6 col-lg-5 col-xl-4 align-self-center">
                        <?php get_template_part('img/icons/logo-3.svg'); ?>
                        <?php $footer_menu_items = wp_get_nav_menu_items('menu-footer'); ?>
                        <table class="footer-menu">
                            <?php $i = 1;
                            foreach ($footer_menu_items as $footer_item) :
                                $menu_url = $footer_item->url;
                                $menu_name = $footer_item->title;
                                if ($i == 1) {
                                    echo '<tr>';
                                }
                                echo '<td class="footer-menu-item"><a href="' . $menu_url . '" class="footer-link">' . $menu_name . '</a></td>';
                                if ($i % 1 == 0) {
                                    echo '</tr>';
                                }
                                $i++; endforeach; ?>
                        </table>
                        <div class="social-items">
                            <?php
                            if ($option_facebook) {
                                echo '<a href="' . $option_facebook . '" target="blank">' . file_get_contents(get_template_directory_uri() . '/img/icons/facebook.svg') . '</a>';
                            }
                            if ($option_twitter) {
                                echo '<a href="' . $option_twitter . '" target="blank">' . file_get_contents(get_template_directory_uri() . '/img/icons/twitter.svg') . '</a>';
                            }
                            if ($option_instagram) {
                                echo '<a href="' . $option_instagram . '" target="blank">' . file_get_contents(get_template_directory_uri() . '/img/icons/instagram.svg') . '</a>';
                            }
                            if ($option_linkedin) {
                                echo '<a href="' . $option_linkedin . '" target="blank">' . file_get_contents(get_template_directory_uri() . '/img/icons/linkedin.svg') . '</a>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class=" col-12 col-sm-2 text-center">
                        <div class="footer-brand">
                            <div class="brand-wrapper">
                                <?php $logo_2 = get_field("logo_2", "option"); ?>
                                <img src=" <?php echo $logo_2['url']; ?>" alt=" <?php echo $logo_2['alt']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center col-sm-4 text-sm-right align-self-start">
                        <div class="company-info">
                            <ul class="info-list">
                                <li>
                                    <?php echo $option_adres; ?>
                                </li>
                                <li>
                                    <?php echo $option_postcode . ' ' . $option_stad; ?>
                                </li>
                                <li>
                                    <a href="tel:<?php echo $option_tel; ?>" class="disable-anim">
                                        <?php echo __('T:', 'webcommitment-theme') . ' ' . $option_tel; ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="mailto:<?php echo $option_mail; ?>" class="disable-anim">
                                        <?php echo __('E:', 'webcommitment-theme') . ' ' . $option_mail; ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-5 col-lg-6 col-xl-6 pt-2 text-sm-center text-lg-left">
                    <div class="footer-bottom__flag">
                        <?php get_template_part('img/icons/flag.svg'); ?>
                    </div>
                </div>
                <div class="col-12 col-md-7 col-lg-5 col-xl-4 pt-3 pb-2 ">
                    <ul class="footer-bottom__links">
                        <li>
                            <a href="/privacy-policy">
                                <?php echo __('Privacy beleid', 'webcommitment-theme'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="/algemene-voorwaarden">
                                <?php echo __('Algemene voorwaarden', 'webcommitment-theme'); ?>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>


        </div>

    </div>

</footer><!-- #colophon -->
</div><!-- #page -->

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
