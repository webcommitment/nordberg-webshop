<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 * Template Name: Page-collectie
 *  Template Post Type: page
 */

get_header();

$children = get_pages(array('child_of' => $post->ID));

?>
    <article>
        <section class="page-banner" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
            <div class="banner-inner">
                <div class="banner-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-11 col-xl-10">
                                <div class="content">
                                    <h1>
                                        <?php echo get_the_title(); ?>
                                    </h1>
                                    <p>
                                        <?php echo get_the_excerpt(); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <?php get_template_part('template-parts/content', 'collectie'); ?>
        </section>
    </article>
    <section id="section-slider">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 pl-0 pr-0">
                    <?php get_template_part('template-parts/content', 'slider'); ?>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
